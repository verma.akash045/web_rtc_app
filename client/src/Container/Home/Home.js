import React, { useState, useEffect, useRef } from "react";
import RecordRTC from 'recordrtc';
import openSocket from 'socket.io-client';
import './Home.css';


const Home = (props) => {

    let [context, setContext] = useState();

    const socket = openSocket('http://localhost:3001/')

    let screenRecorder; // globally accessible
    let camRecorder;

    let video = useRef(null)
    let cameraVideo = useRef(null)
    let canvas = useRef(null)
    let start = useRef(null)
    let stop = useRef(null)

    useEffect(() => {
        let temp = canvas.current.getContext('2d')
        console.log("tttemp  -->", temp)
        setContext(temp)
      } );

    let invokeGetDisplayMedia = (success, error) => {
        var displaymediastreamconstraints = {
            video: {
                displaySurface: 'monitor', // monitor, window, application, browser
                logicalSurface: true,
                cursor: 'always' // never, always, motion
            }
        };

        // above constraints are NOT supported YET
        // that's why overridnig them
        displaymediastreamconstraints = {
            video: true
        };

        if(navigator.mediaDevices.getDisplayMedia) {
            navigator.mediaDevices.getDisplayMedia(displaymediastreamconstraints).then((screenStream)=>{
                navigator.mediaDevices.getUserMedia({audio:true}).then(function(mic) {
                    screenStream.addTrack(mic.getTracks()[0]);
                    success(screenStream);
                })
            }).catch(error);
        }
        else {
            navigator.getDisplayMedia(displaymediastreamconstraints).then(success).catch(error);
        }
    }

    let addStreamStopListener = (stream, callback) => {
        stream.addEventListener('ended', function() {
            callback();
            callback = function() {};
        }, false);
        stream.addEventListener('inactive', function() {
            callback();
            callback = function() {};
        }, false);
        stream.getTracks().forEach(function(track) {
            track.addEventListener('ended', function() {
                callback();
                callback = function() {};
            }, false);
            track.addEventListener('inactive', function() {
                callback();
                callback = function() {};
            }, false);
        });
    }


    let captureScreen = (callback) => {
        invokeGetDisplayMedia(function(screen) {
            addStreamStopListener(screen, function() {
                document.getElementById('btn-stop-recording').click();
            });
            callback(screen);
        }, function(error) {
            console.error(error);
            alert('Unable to capture your screen. Please check console logs.\n' + error);
        });
    }

    let viewVideo = (video, context) =>{
        context.drawImage(video, 0, 0, 400,400);
        socket.emit('stream', canvas.current.toDataURL('image/webp'))
    }

    let captureCamera = (callback) => {
        navigator.mediaDevices.getUserMedia({ audio: true, video: true }).then(stream => {
          setInterval(() =>{
              viewVideo(video.current, context)
          }, 5)

           callback(stream);
        }).catch(function(error) {
            alert('Unable to capture your camera. Please check console logs.\n' + error);
            console.error("error ->", error);
        });
    }

    let startRecording = () => {
        start.current.disabled = true;
        captureScreen(function(screen) {
            video.current.muted = true;
            video.current.autoplay = true;
            video.current.srcObject = screen;
            screenRecorder = RecordRTC(screen, {
            type: 'video'
            });
        screenRecorder.startRecording();
        // release screen on stopRecording
        screenRecorder.screen = screen;
        stop.current.disabled = false;
        });

        captureCamera(function(camera) {
            cameraVideo.current.muted = true;
            cameraVideo.current.autoplay = true;
            cameraVideo.current.volume = 0;
            cameraVideo.current.srcObject = camera;

            camRecorder = RecordRTC(camera, {
                type: 'video'
            });

            camRecorder.startRecording();

            // release camera on stopRecording
            camRecorder.camera = camera;
        });
    }

    let stopRecordingCallback =() => {

        video.current.src = video.current.srcObject = null;
        video.current.src = URL.createObjectURL(screenRecorder.getBlob());
        screenRecorder.screen.stop();
        screenRecorder.destroy();
        screenRecorder = null;

        start.current.disabled = false;
    }


    let stopCamRecordingCallback = () =>{
        cameraVideo.src = cameraVideo.srcObject = null;
        cameraVideo.muted = false;
        cameraVideo.volume = 1;
        cameraVideo.src = URL.createObjectURL(camRecorder.getBlob());
    
        camRecorder.camera.stop();
        camRecorder.destroy();
        camRecorder = null;
    }


    let stopRecording = () => {
        // stop.current.disabled = true;
        screenRecorder.stopRecording(stopRecordingCallback());
        camRecorder.stopRecording(stopCamRecordingCallback());
    }

    return (
        <div>  
            <title>Screen Recording | RecordRTC</title>
            <h1>Screen Recording using RecordRTC</h1>
            <button ref={start} onClick={() => startRecording()}>Start Recording</button>
            <button ref={stop} onClick={() => stopRecording()} >Stop Recording</button>
            <hr/> 
            <video ref={video} controls autoplay ></video>
            <video ref={cameraVideo} controls autoplay ></video>
            <canvas ref={canvas}> </canvas>
        </div>
    );
}


export default Home;
