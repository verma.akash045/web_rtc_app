const mongoose = require("mongoose");
const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");
const socketIO = require('socket.io')
const app = express();
const server = http.createServer(app)
const io = socketIO(server)
app.use(express.static(__dirname + "/public"));

const API_PORT = 3001;
let broadcaster;

mongoose.connect('mongodb://localhost:27017/test');
let db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

io.on('connection', socket =>{
	console.log("connected")

  	socket.on("disconnect", () => {
    	console.log("disconnected")
  	});

  	socket.on("message", () => {
    	console.log("message")
  	});

  	socket.on("stream", data => {
    	console.log("data stream ->", data)
  	});

})


app.get("/", (req, res) => {
	res.json({ message: "HELLOW" });
});

server.listen(API_PORT, () => console.log(`LISTENING ON UHH PORT ${API_PORT}`))
